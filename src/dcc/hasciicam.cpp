/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Bernd Buschinski <b.buschinski@googlemail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "hasciicam.h"

#include <iostream>

#include <QTimer>

#include <KDebug>

using namespace Konversation::DCC;

HAsciiCam::HAsciiCam(QWidget* parent)
    : QTextEdit(parent),
      m_timer(0)
{
    initCam();
    setReadOnly(true);
    setFontPointSize(7);
    setFontFamily("DejaVu Sans Mono");
    m_skips = 0;
}

HAsciiCam::~HAsciiCam()
{
    if (m_timer)
        m_timer->stop();
    delete m_timer;

    if (m_noCam)
        return;

    // turn off streaming
    if(-1 == ioctl(fd, VIDIOC_STREAMOFF, &buftype))
    {
        perror("VIDIOC_STREAMOFF");
    }

    /* Cleanup. */
    for (unsigned int i = 0; i < reqbuf.count; ++i)
        munmap (buffers[i].start, buffers[i].length);

    aa_close(ascii_context);
    free(grey);
    if(fd > 0)
        ::close(fd);
    fprintf (stderr, "cya!\n");
}

void HAsciiCam::connected()
{
}

void HAsciiCam::receivedHAsciiCamLine(const QString& line)
{
    clear();

    QStringList args = line.split(' ');
    kDebug() << args;
    if (args[0] != "NEWPIC")
    {
        kDebug() << args[0];
        return;
    }

    int pos = line.indexOf(' '); // space after newpic
    pos = line.indexOf(' ', pos+1); // space after width
    pos = line.indexOf(' ', pos+1); // space after height

    unsigned textwidth = args[1].toUInt();
    unsigned textheight = args[2].toUInt();
    QString text = line.right(line.size() - (pos+1));
    kDebug() << text.length();

    for (unsigned i = 0; i < textheight; ++i)
    {
        QString appText = text.left(textwidth); //.replace('<', "&#60;").replace('>', "&#62;").replace('#', "&#35;");

        append(appText);
        kDebug() << appText;
        kDebug() << "";
        text = text.right(text.length() - textwidth);
    }
    update();

//     std::cout << line.toAscii().data() << std::endl;
}

void HAsciiCam::YUV422_to_grey(unsigned char* src, unsigned char* dst, int w, int h)
{
    unsigned char *writehead;
    unsigned char *readhead;
    int x,y;
    writehead = dst;
    readhead  = src;
    for(y=0; y<gh; ++y)
    {
        for(x=0; x<gw; ++x)
        {
            *(writehead++) = *readhead;
            readhead += xbytestep;
        }
        readhead += ybytestep;
    }
}

void HAsciiCam::grabOne()
{
    kDebug();

    // Can we have a buffer please?
    if (-1 == ioctl (fd, VIDIOC_DQBUF, &buffer))
    {
        perror ("VIDIOC_DQBUF");
        return;
    }

    YUV422_to_grey((unsigned char*)buffers[buffer.index].start, grey, vw, vh);

    memcpy(aa_image(ascii_context), grey, greysize);
    aa_render(ascii_context, ascii_rndparms, 0, 0, vw/(xstep*2), vh/(ystep*2)); //TODO are the w&h args correct?
    aa_flush(ascii_context);

    // Thanks for lending us your buffer, you may have it back again:
    if (-1 == ioctl (fd, VIDIOC_QBUF, &buffer))
    {
        perror ("VIDIOC_QBUF");
        return;
    }

    if (m_skips < 12)
    {
        ++m_skips;
        return;
    }
    m_skips = 0;

    std::cout << "===== NEW ====" << std::endl;

    unsigned char* text = aa_text(ascii_context);
    unsigned textwidth = aa_scrwidth(ascii_context);
    unsigned textheight = aa_scrheight(ascii_context);
//     std::cout << textwidth << std::endl;
//     std::cout << textheight << std::endl;
//     std::cout << text << std::endl;

    QString resultText = QString::fromAscii((char*)text, textwidth*textheight);

    QString cmd;
    cmd += "\x01""NEWPIC ";
    cmd += QString::number(textwidth);
    cmd += " ";
    cmd += QString::number(textheight);
    cmd += " ";
    cmd += resultText;
    cmd += "\x01";

    emit sendHAsciiCamLine(cmd);
}

void HAsciiCam::initCam()
{
    memcpy (&ascii_hwparms, &aa_defparams, sizeof (struct aa_hardware_params));
    ascii_rndparms = aa_getrenderparams();

    { /* device filename */
        struct stat st;
        if( stat("/dev/video",&st) <0)
            strcpy(device,"/dev/video0");
        else
            strcpy(device,"/dev/video");
    }

    aa_geo.w = 80; // 96;
    aa_geo.h = 40; // 72;
    aa_geo.bright = 40;
    aa_geo.contrast = 4;
    aa_geo.gamma = 3;

    buftype = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    fd = -1;
    inputch = 0;

    if (vid_detect(device) > 0)
    {
        if (vid_init() < 0)
            m_noCam = true;
        else
            m_noCam = false;
    }
    else
        m_noCam = true;

    if (m_noCam)
        return;

    /* width/height image setup */
    ascii_hwparms.font = NULL; // default font, thanks
    ascii_hwparms.width = aw;
    ascii_hwparms.height = ah;

    strcpy(aafile,"hasciicam.asc");
    ascii_save.name = aafile;
    ascii_save.format = &aa_text_format;
    ascii_save.file = NULL;

    ascii_context = aa_init (&mem_d, &ascii_hwparms, &ascii_save);

    if (!ascii_context)
    {
        fprintf(stderr,"!! cannot initialize aalib\n");
        m_noCam = true;
        return;
    }

    ascii_rndparms->bright = aa_geo.bright;
    ascii_rndparms->contrast = aa_geo.contrast;
    ascii_rndparms->gamma = aa_geo.gamma;

    m_timer = new QTimer();
    m_timer->setInterval(41);
    connect (m_timer, SIGNAL(timeout()), this, SLOT(grabOne()));
    m_timer->start();
}

int HAsciiCam::vid_detect(char* devfile)
{
    int errno;

    if (-1 == (fd = open(devfile,O_RDWR|O_NONBLOCK))) {
        perror("!! error in opening video capture device: ");
        return -1;
    } else {
        ::close(fd);
        fd = open(devfile,O_RDWR);
    }

    // Check that streaming is supported
    memset(&capability, 0, sizeof(capability));
    if (-1 == ioctl(fd, VIDIOC_QUERYCAP, &capability))
    {
        perror("VIDIOC_QUERYCAP");
        return -1;
    }

    if ((capability.capabilities & V4L2_CAP_VIDEO_CAPTURE) == 0)
    {
        printf("Fatal: Device %s does not support video capture\n", capability.card);
        return -1;
    }

    if((capability.capabilities & V4L2_CAP_STREAMING) == 0)
    {
        printf("Fatal: Device %s does not support streaming data capture\n", capability.card);
        return -1;
    }

    fprintf(stderr,"Device detected is %s\n",devfile);
    fprintf(stderr,"Card name: %s\n",capability.card);

    // Switch to the selected video input
    if (-1 == ioctl(fd, VIDIOC_S_INPUT, &inputch))
    {
        perror("VIDIOC_S_INPUT");
        return -1;
    }

    // Get info about current video input
    memset(&input, 0, sizeof(input));
    input.index = inputch;
    if (-1 == ioctl(fd, VIDIOC_ENUMINPUT, &input))
    {
        perror("VIDIOC_ENUMINPUT");
        return -1;
    }
    printf("Current input is %s\n", input.name);
    // example 1-6
    memset(&standard, 0, sizeof(standard));
    standard.index = 0;
    while (0 == ioctl (fd, VIDIOC_ENUMSTD, &standard))
    {
        if(standard.id & input.std)
            printf("   - %s\n", standard.name);
        standard.index++;
    }
    // Need to find out (request?) specific data format (sec 1.10.1)
    memset(&format, 0, sizeof(format));
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    //noeffect   format.fmt.pix.width  = 320;
    //noeffect   format.fmt.pix.height = 240;
    if (-1 == ioctl(fd, VIDIOC_G_FMT, &format))
    {
        perror("VIDIOC_G_FMT");
        return -1;
    }
    // the format.fmt.pix member, a v4l2_pix_format, is now filled in
    printf("Current capture is %u x %u\n",
           format.fmt.pix.width, format.fmt.pix.height);
    printf("format %4.4s, %u bytes-per-line\n", 
           (char*)&format.fmt.pix.pixelformat,
           format.fmt.pix.bytesperline);

    return 1;
}

int HAsciiCam::vid_init()
{
    unsigned int i, j;

    xstep = 2; //2 - 4
    ystep = 4; //4 - 8

    vw = format.fmt.pix.width;
    vh = format.fmt.pix.height;
    vbytesperline = format.fmt.pix.bytesperline;
    xbytestep = xstep + xstep; // for YUV422. for other formats may differ
    ybytestep = vbytesperline * (ystep-1);
    // we shrink our pixels crudely, by hopping over them:
    gw = vw / xstep;
    gh = vh / ystep;
    // aalib converts every block of 4 pixels to one character, so our sizes shrink by 2:
    aw = gw / 2;
    ah = gh / 2;
    std::cout << "AW:" << aw << " AH:" << ah << std::endl;

    greysize = gw * gh;
    grey = (unsigned char*)malloc(greysize); // To get grey from YUYV we simply ignore the U and V bytes
    printf("Grey buffer is %u bytes\n", greysize);
    for (j=0; j< 256; ++j)
        YtoRGB[j] = 1.164*(j-256);

    memset (&reqbuf, 0, sizeof (reqbuf));
    reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbuf.memory = V4L2_MEMORY_MMAP;
    reqbuf.count = 32;

    if (-1 == ioctl (fd, VIDIOC_REQBUFS, &reqbuf))
    {
        if (errno == EINVAL)
            printf ("Fatal: Video capturing by mmap-streaming is not supported\n");
        else
            perror ("VIDIOC_REQBUFS");

        return -1;
    }
    buffers = (v4l2_data*)calloc (reqbuf.count, sizeof (*buffers));
    if (buffers == NULL)
    {
        printf("calloc failure!");
        return -1;
    }

    for (i = 0; i < reqbuf.count; ++i)
    {
        memset (&buffer, 0, sizeof (buffer));
        buffer.type = reqbuf.type;
        buffer.memory = V4L2_MEMORY_MMAP;
        buffer.index = i;

        if (-1 == ioctl (fd, VIDIOC_QUERYBUF, &buffer))
        {
            perror ("VIDIOC_QUERYBUF");
            return -1;
        }

        buffers[i].length = buffer.length; /* remember for munmap() */

        buffers[i].start = mmap (NULL, buffer.length,
                                 PROT_READ | PROT_WRITE, /* recommended */
                                 MAP_SHARED,             /* recommended */
                                 fd, buffer.m.offset);

        if (MAP_FAILED == buffers[i].start)
        {
            /* If you do not exit here you should unmap() and free()
             *                    the buffers mapped so far. */
            perror ("mmap");
            return -1;
        }
    }
    /* OK, the memory is mapped, so let's queue up the buffers,
       next is: turn on streaming, and do the business. */

    for (i = 0; i < reqbuf.count; ++i)
    {
        // queue up all the buffers for the first time
        memset (&buffer, 0, sizeof (buffer));
        buffer.type = reqbuf.type;
        buffer.memory = V4L2_MEMORY_MMAP;
        buffer.index = i;

        if (-1 == ioctl (fd, VIDIOC_QBUF, &buffer))
        {
            perror ("VIDIOC_QBUF");
            return -1;
        }
    }

    // turn on streaming
    if (-1 == ioctl(fd, VIDIOC_STREAMON, &buftype))
    {
        perror("VIDIOC_STREAMON");
        return -1;
    }

    for (i = 0; i < greysize; ++i)
    {
        grey[i] = i % 160; //256;
    }
    return 0;
}
