/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Bernd Buschinski <b.buschinski@googlemail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef HASCIICAM_H
#define HASCIICAM_H

#include <QTextEdit>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <signal.h>

#include <aalib.h>
#include <linux/types.h>
#include <linux/videodev2.h>

namespace Konversation {
namespace DCC {

class HAsciiCam : public QTextEdit
{
    Q_OBJECT;
    
public:
    HAsciiCam(QWidget* parent = 0);
    virtual ~HAsciiCam();

public slots:
    void connected();
    
    void receivedHAsciiCamLine(const QString& line);

signals:
    void sendHAsciiCamLine(const QString& line);

protected slots:
    void grabOne();

private:
    void initCam();
    int vid_detect(char* devfile);
    int vid_init();

    inline void YUV422_to_grey(unsigned char *src, unsigned char *dst, int w, int h);

    aa_context *ascii_context;
    struct aa_renderparams *ascii_rndparms;
    struct aa_hardware_params ascii_hwparms;
    struct aa_savedata ascii_save;

    struct geometry {
      int w, h, size;
      int bright, contrast, gamma;
    };
    struct geometry aa_geo;
    struct geometry vid_geo;

    char device[256];

    int buftype;
    struct v4l2_capability capability;
    struct v4l2_input input;
    struct v4l2_standard standard;
    struct v4l2_format format;
    struct v4l2_requestbuffers reqbuf;
    struct v4l2_buffer buffer;
    struct v4l2_data {
        void *start;
        size_t length;
    } *buffers;

    int fd;

    int inputch;
    bool m_noCam;

    int gw, gh; // number of cols/rows in grey intermediate representation
    int vw, vh; // video w and h
    int aw, ah; // ascii w and h

    int xbytestep;
    int ybytestep;
    int vbytesperline;

    int xstep, ystep;

    size_t greysize;

    /* greyscale image is sampled from Y luminance component */
    unsigned char *grey;
    int YtoRGB[256];

    char aafile[4096];
    
    QTimer* m_timer;
    
    unsigned m_skips;
};

}
}

#endif // HASCIICAM_H
