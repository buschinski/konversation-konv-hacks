/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

/*
  Copyright (C) 2009 Bernd Buschinski <b.buschinski@web.de>
*/

#ifndef HASCICAMGLOBALS_H
#define HASCICAMGLOBALS_H

#include <QHash>

namespace Konversation
{
    namespace DCC
    {
        class HAsciiCamGlobals
        {
            public:
                enum HAsciiCamCommand
                {
                    NEWPIC = 1
                };

                static QHash<QString, HAsciiCamCommand> hasciicamCommandHash()
                {
                    static QHash<QString, HAsciiCamCommand> hasciicamCommands;
                    if (!hasciicamCommands.isEmpty())
                    {
                        return hasciicamCommands;
                    }
                    hasciicamCommands.insert(QLatin1String("NEWPIC"), NEWPIC);
                    return hasciicamCommands;
                }
        };
    }
}

#endif // HASCICAMGLOBALS_H
